const mix = require('laravel-mix');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ImageminPlugin =  require('imagemin-webpack-plugin').default;
const CopyWebpackPlugin  = require('copy-webpack-plugin');
const imageminMozjpeg    = require('imagemin-mozjpeg');


mix.setPublicPath('dist')
    .autoload({
        jquery: ['$', 'window.jQuery']
    })
    .js('src/js/script.js', 'js/')
    .sass('src/scss/style.scss', 'css/')
    .options({
        processCssUrls: false // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
    });

if (!mix.inProduction()) {

    mix.sourceMaps()
        .webpackConfig({
            devServer: {
                hot: true, // this enables hot reload
                inline: true, // use inline method for hmr
                host: "localhost",
                port: 8080,
                open: true,
                contentBase: path.join(__dirname, "dist/"), // should point to the laravel public folder
            },
            plugins: [
                new CopyWebpackPlugin([{
                    from: 'src/images', // FROM
                    to: 'images', // TO
                }]),
                new HtmlWebpackPlugin({
                    template: 'src/html/index.html'
                })
            ],
            devtool :  "inline-source-map"
        });
} else {

    mix.webpackConfig({
        plugins: [
            new CopyWebpackPlugin([{
                from: 'src/images', // FROM
                to: 'images', // TO
            }]),
            new HtmlWebpackPlugin({
                template: 'src/html/index.html'
            }),
            new ImageminPlugin({
                test: /\.(jpe?g|png|gif|svg)$/i,
                pngquant: {
                    quality: '65-80'
                },
                plugins: [
                    imageminMozjpeg({
                        quality: 65,
                        //Set the maximum memory to use in kbytes
                        maxMemory: 1000 * 512
                    })
                ]
            })
        ]
    }).version();
}
